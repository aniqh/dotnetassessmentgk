﻿using CoinJarApp.Common.Enumerations;
using CoinJarApp.Core;
using CoinJarApp.Core.Helpers;
using CoinJarApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CoinJarApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var coinJar = new CoinJar();
            var coinGenerator = new CoinGenerator();
            var input = "";
            Console.WriteLine(coinJar);

            while(input != "q")
            {
                Console.WriteLine();
                Console.WriteLine("Please enter An Option: \n\ta [coin type] - add coin\n\tr - reset coin jar\n\tq - quit\n\n\teg: a dollar");
                Console.WriteLine("\tCoin Types: penny, nickel, dime, quarter, half, dollar");
                input = Console.ReadLine();
                Console.Clear();

                switch (input.Split(" ")[0])
                {
                    case "q":                        
                        Console.WriteLine("Exiting Application...");
                        break;

                    case "r":                      
                        coinJar.Reset();
                        Console.WriteLine(coinJar);
                        break;

                    case "a":
                        if (!(input.Split(" ").Length > 1))
                        {
                            Console.WriteLine("Invalid syntax: " + input);
                            break;
                        }

                        switch(input.Split(" ")[1])
                        {
                            case "dime":
                                coinJar.AddCoin(coinGenerator.generateCoin(CoinType.Dime));
                                break;

                            case "nickel":
                                coinJar.AddCoin(coinGenerator.generateCoin(CoinType.Nickel));
                                break;

                            case "penny":
                                coinJar.AddCoin(coinGenerator.generateCoin(CoinType.Penny));
                                break;

                            case "quarter":
                                coinJar.AddCoin(coinGenerator.generateCoin(CoinType.Quarter));
                                break;

                            case "half":
                                coinJar.AddCoin(coinGenerator.generateCoin(CoinType.HalfDollar));
                                break;

                            case "dollar":
                                coinJar.AddCoin(coinGenerator.generateCoin(CoinType.Dollar));
                                break;

                            default:
                                Console.WriteLine("Invalid option: " + input.Split(" ")[1]);
                                Console.WriteLine("Availabile coin types: nickel, dime, penny, quarter, half");
                                break;
                        }

                        Console.WriteLine();
                        Console.WriteLine(coinJar);
                        break;

                    default:                      
                        Console.WriteLine("Invalid syntax: " + input);
                        break;
                }
            }
        }

        
    }
}
