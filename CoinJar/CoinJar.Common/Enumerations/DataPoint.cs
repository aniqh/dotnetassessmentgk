﻿using System.ComponentModel;

namespace CoinJarApp.Common.Enumerations
{
    public enum CoinType : byte
    {
        [Description("Penny")]
        Penny = 1,
        [Description("Nickel")]
        Nickel = 2,
        [Description("Dime")]
        Dime = 3,
        [Description("Quarter")]
        Quarter = 4,
        [Description("HalfDollar")]
        HalfDollar = 5,
        [Description("Dollar")]
        Dollar = 6
    };
}
