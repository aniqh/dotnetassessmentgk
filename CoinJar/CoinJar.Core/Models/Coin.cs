﻿using CoinJarApp.Core.Contract;
using System;

namespace CoinJarApp.Core.Models
{
    public class Coin : ICoin
    {
        public decimal Amount { get; set; }
        public decimal Volume { get; set; }

        public Coin(double amount, double volume)
        {
            Amount = Convert.ToDecimal(amount);
            Volume = Convert.ToDecimal(volume);
        }
    }
}
