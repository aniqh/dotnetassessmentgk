﻿using CoinJarApp.Core.Contract;
using System;

namespace CoinJarApp.Core.Models
{
	public class CoinJar : ICoinJar
	{
		/// <summary>
		/// Set up values for Jar volume and error margin (gaps between coins)
		/// </summary>
		public const decimal MAX_VOLUME_VALUE = 42;
		public const decimal VOLUME_ERROR_MODIFIER = 0.95M;

		public decimal TotalAmount { get; private set; }
		private decimal CurrentVolume { get; set; }
		private decimal MaxVolume { get; set; }

		/// <summary>
		/// Instantiate class variables
		/// </summary>
		public CoinJar()
		{
			TotalAmount = 0;
			CurrentVolume = 0;
			MaxVolume = MAX_VOLUME_VALUE * VOLUME_ERROR_MODIFIER;
		}

		/// <summary>
		/// Take in an instance on ICoin
		/// Check if there is space in the jar
		/// Update total jar value and volume if there is space
		/// </summary>
		/// <param name="coin"></param>
		public void AddCoin(ICoin coin)
		{
			if (coin == null) return;
			if (CurrentVolume + coin.Volume > MaxVolume)
			{
				Console.WriteLine("Coin jar exceeded capacity");
				return;
			}
			TotalAmount += coin.Amount;
			CurrentVolume += coin.Volume;
			Console.WriteLine("$" + string.Format("{0:0.00}", coin.Amount) + " Added");
		}

		/// <summary>
		/// Empties coin jar, resets stored amt & volume
		/// </summary>
		public void Reset()
		{
			Console.WriteLine("Resetting CoinJar...");
			TotalAmount = 0;
			CurrentVolume = 0;
		}

		/// <summary>
		/// Returns the current used volume of the jar
		/// </summary>
		public decimal GetCurrentVolume()
		{
			return CurrentVolume;
		}

		/// <summary>
		/// Returns summary of current status of the jar
		/// -Current value
		/// -Current Volume
		/// -Total Jar Volume
		/// </summary>
		public override string ToString()
		{
			return "Total Collected Amount: $" + string.Format("{0:0.00}", TotalAmount) + " - Current Volume: " + CurrentVolume + " - Max Volume: " + MaxVolume;
		}
	}
}
