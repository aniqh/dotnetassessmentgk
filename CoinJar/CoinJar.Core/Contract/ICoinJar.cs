﻿namespace CoinJarApp.Core.Contract
{
    public interface ICoinJar
    {
        void AddCoin(ICoin coin);
        decimal TotalAmount { get; }
        void Reset();
    }
}
