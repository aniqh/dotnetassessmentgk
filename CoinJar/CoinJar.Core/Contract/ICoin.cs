﻿namespace CoinJarApp.Core.Contract
{
    public interface ICoin
    {
        decimal Amount { get; }
        decimal Volume { get; }
    }
}
