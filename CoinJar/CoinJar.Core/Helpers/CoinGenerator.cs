﻿using CoinJarApp.Common.Enumerations;
using CoinJarApp.Core.Contract;
using CoinJarApp.Core.Models;
using System.Collections.Generic;

namespace CoinJarApp.Core.Helpers
{
    public class CoinGenerator
    {
        private Dictionary<CoinType, ICoin> coinSet;

        /// <summary>
        /// Set up coin dictionary w/ monetary value and volume
        /// </summary>
        public CoinGenerator()
        {
            coinSet = new Dictionary<CoinType, ICoin>(){
                { CoinType.Penny, new Coin(0.01, 0.014649415) },
                { CoinType.Nickel, new Coin(0.05, 0.023297143) },
                { CoinType.Dime, new Coin(0.1, 0.011500366) },
                { CoinType.Quarter, new Coin(0.25, 0.027353088) },
                { CoinType.HalfDollar, new Coin(0.5, 0.053499761) },
                { CoinType.Dollar, new Coin(1, 0.037299968) },
            };
        }

        /// <summary>
        /// Coin generator, returns ICoin based on dictionary lookup by CoinType
        /// </summary>
        /// <param name="type"></param>
        public ICoin generateCoin(CoinType type)
        {
            return coinSet.GetValueOrDefault(type);
        }
    }
}
