﻿using CoinJarApp.Common.Enumerations;
using CoinJarApp.Core.Contract;
using CoinJarApp.Core.Helpers;
using CoinJarApp.Core.Models;
using System.Collections.Generic;
using Xunit;

namespace CoinJarApp.Test
{
    public class CoinJarTest
    {
        [Fact]
        public void CoinJar_AddCoin()
        {
            // Arrange
            var coinGenerator = new CoinGenerator();
            var coinjar = new CoinJar();
            var coin = coinGenerator.generateCoin(CoinType.Dollar);

            // Act
            coinjar.AddCoin(coin);

            // Assert
            Assert.True(coinjar.TotalAmount != 0);
            Assert.Equal(coin.Amount, coinjar.TotalAmount);
        }

        [Fact]
        public void CoinJar_AddCoin_Overflow()
        {
            // Arrange
            var coinGenerator = new CoinGenerator();
            var coinjar = new CoinJar();

            // Act
            for (int i = 0; i < 1000; i++)
            {
                coinjar.AddCoin(coinGenerator.generateCoin(CoinType.HalfDollar));
            }

            // Assert
            Assert.True(coinjar.GetCurrentVolume() <= 42);
        }

        [Fact]
        public void CoinJar_GetCurrentVolume()
        {
            // Arrange
            var coinGenerator = new CoinGenerator();
            var coinjar = new CoinJar();
            var coin1 = coinGenerator.generateCoin(CoinType.Dollar);
            var coin2 = coinGenerator.generateCoin(CoinType.HalfDollar);

            // Act
            coinjar.AddCoin(coin1);
            coinjar.AddCoin(coin2);

            // Assert
            Assert.Equal((coin1.Volume+coin2.Volume), coinjar.GetCurrentVolume());
        }

        [Fact]
        public void CoinJar_Reset()
        {
            // Arrange
            var coinGenerator = new CoinGenerator();
            var coinjar = new CoinJar();
            var coins = new List<ICoin>()
            {
                coinGenerator.generateCoin(CoinType.Dollar),
                coinGenerator.generateCoin(CoinType.Dollar),
                coinGenerator.generateCoin(CoinType.HalfDollar),
                coinGenerator.generateCoin(CoinType.Nickel),
                coinGenerator.generateCoin(CoinType.Penny),
                coinGenerator.generateCoin(CoinType.Quarter)
            };


            // Act
            foreach (var c in coins)
            {
                coinjar.AddCoin(c);
            }
            coinjar.Reset();


            // Assert
            Assert.Equal(0, coinjar.TotalAmount);
        }

        [Fact]
        public void CoinJar_toString()
        {
            // Arrange
            var coinjar = new CoinJar();

            // Act
            var output = coinjar.ToString();

            // Assert
            Assert.Contains("Total", output);
        }
    }
}
