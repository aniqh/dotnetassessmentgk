# .NET Assessment GK #



## Task 1: Implement C# StingCollection ##


### Create a C# class derived from Object and implements the following ###

* AddString – This method should add a given string to an internal collection
* ToString – Override this method and return a single, comma-delimited string containing all the strings in the internal collection

### Submitted Classes ###

* IStringCollection.cs
* StringColletion.cs



## Task 2: Implement a coin jar in C# ##


### The coin jar will only accept the latest US coinage and has a volume of 42 fluid ounces. Additionally, the jar has a counter to keep track of the total amount of money collected and has the ability to reset the count back to $0.00. ###

### Submitted Project: CoinJar ###
* Built in VS 2019
* Open CoinJar.sln
* Build and run CoinJarApp project
* Follow console instructions to add coins to jar
* Specified interfaces found at CoinJarApp.Core/Contract

#### e.g user inputs ####
a dollar  
a dime  
a nickel  
r  
a penny  
q