﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoinJarApp
{
    public interface IStringCollection
    {
        void AddString(string s);
        string ToString();
    }
}
