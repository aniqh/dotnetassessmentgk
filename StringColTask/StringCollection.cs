﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CoinJarApp
{
    public class StringCollection : IStringCollection
    {
        private List<string> stringCollection;

        public StringCollection()
        {
            stringCollection = new List<string>();
            Thread.MemoryBarrier();
        }

        public void AddString(string s)
        {
            while (true)
            {
                Thread.MemoryBarrier();
                var globalCollection = stringCollection;
                Thread.MemoryBarrier();

                var localCollection = globalCollection.ToList();
                localCollection.Add(s);

                if (Interlocked.CompareExchange(ref stringCollection, localCollection, globalCollection) == globalCollection) break;
            }
        }

        public override string ToString()
        {
            Thread.MemoryBarrier();
            return string.Join(",", stringCollection);
        }
    }
}
